package com.song.autodispose.net;

import com.song.autodispose.net.entity.BaseEntity;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.POST;

public interface TestService {
    /**
     *
     */
    @POST("")
    Observable<BaseEntity<Map<String, Map<String, String>>>> getResourceClassificationStatistic();
}
