package com.song.autodispose.net.entity;

import com.google.gson.annotations.SerializedName;


public class BaseEntity<T> extends HttpStatus {

    @SerializedName("data")
    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
