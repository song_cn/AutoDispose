package com.song.autodispose.net.entity;

import java.io.Serializable;
import java.util.Map;

/**
 * 分页接口请求参数
 * @author wuc
 * @date 2019/4/17
 */
public class PageRequest implements Serializable {

    private int pageSize = 10; // 默认每页10条

    private int currentPage = 1; // 默认第一页

    public PageRequest() {
    }

    public PageRequest(int pageSize, int currentPage) {
        this.pageSize = pageSize;
        this.currentPage = currentPage;
    }

    public PageRequest(Map<Object, Object> condition) {
        this.condition = condition;
    }

    private Map<Object, Object> condition; // 条件

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public Map<Object, Object> getCondition() {
        return condition;
    }

    public void setCondition(Map<Object, Object> condition) {
        this.condition = condition;
    }

}
