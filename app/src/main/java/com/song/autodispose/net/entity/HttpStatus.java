package com.song.autodispose.net.entity;

import com.google.gson.annotations.SerializedName;

/**
 * 创建时间: 2017/5/9 <br />
 * 编写人: 吴闯 <br />
 * 功能描述:
 */
public class HttpStatus {

    public interface STATUS_CODE {
        int CODE_SUCCESS = 200;
    }

    /**
     * 状态码 200:成功，其它错误
     */
    @SerializedName("code")
    private int code;

    /**
     * 接口描述
     */
    @SerializedName("msg")
    private String msg;

    /**
     * 状态码 是否正常
     */
    public boolean isStatusNormal() {
        return code == STATUS_CODE.CODE_SUCCESS;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
