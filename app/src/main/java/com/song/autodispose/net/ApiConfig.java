package com.song.autodispose.net;


import android.Manifest;

public class ApiConfig {
    public static final String[] Permission = {
            Manifest.permission.INTERNET,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.USE_FINGERPRINT,
            Manifest.permission.WRITE_APN_SETTINGS,
            Manifest.permission.CHANGE_NETWORK_STATE,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.NFC,
            Manifest.permission.CAMERA,
            Manifest.permission.VIBRATE
    };

    public static final String BASE_URL = "";
}
