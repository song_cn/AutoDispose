package com.song.autodispose;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.song.autodispose.callback.RequestCallBack;
import com.song.autodispose.net.RetrofitServiceManager;
import com.song.autodispose.net.TestService;
import com.song.autodispose.rx.ResultFilter;
import com.song.autodispose.rx.RxScheduler;
import com.song.autodispose.util.RxLifecycleUtils;

import java.util.Map;

import io.reactivex.disposables.Disposable;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RetrofitServiceManager.getInstance().create(TestService.class)
                .getResourceClassificationStatistic()
                .map(new ResultFilter<Map<String, Map<String, String>>>())
                .compose(RxScheduler.<Map<String, Map<String, String>>>ioMain())
                .as(RxLifecycleUtils.<Map<String, Map<String, String>>>bindLifecycle(this))
                .subscribe(new RequestCallBack<Map<String, Map<String, String>>>(this) {
                    @Override
                    public void onBefore(Disposable d) {

                    }

                    @Override
                    public void onSuccess(Map<String, Map<String, String>> data) {

                    }
                });
    }
}
