package com.song.autodispose.rx;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

/**
 * 创建时间: 2017/5/12 <br />
 * 编写人: 吴闯 <br />
 * 功能描述:
 */
public class RxRun {

    public static Disposable runOnUiThread(final Action action) {
        return Completable.fromAction(action)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    public static Disposable runOnIOThread(final Action action) {
        return Completable.fromAction(action)
                .observeOn(Schedulers.io())
                .subscribe();
    }

}
