package com.song.autodispose.rx;


import com.song.autodispose.net.entity.BaseEntity;

import io.reactivex.functions.Function;

/**
 * 创建时间: 2017/2/22 <br />
 * 编写人: 吴闯 <br />
 * 功能描述: 请求结果的统一处理。只有请求成功是才会到业务层。
 */
public class ResultFilter<T> implements Function<BaseEntity<T>, T> {

    @Override
    public T apply(BaseEntity<T> baseEntity) throws Exception {
        return baseEntity.getData();
    }

}
