package com.song.autodispose.exception;

/**
 * 创建时间: 2017/2/22 <br />
 * 编写人: 吴闯 <br />
 * 功能描述:
 */
public class ApiException extends RuntimeException {

    private int errorCode;

    public ApiException(int errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }

}