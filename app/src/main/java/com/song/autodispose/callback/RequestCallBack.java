package com.song.autodispose.callback;

import android.content.Context;

import com.song.autodispose.util.NetUtils;
import com.song.autodispose.util.ToastUtils;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public abstract class RequestCallBack<T> implements Observer<T> {

    private final Context context;
    private Disposable disposable;

    public RequestCallBack(Context ctx) {
        this.context = ctx;
    }

    /**
     * 订阅时回调
     * @param d 用来终止订阅关系,由调用者处理,暴露出来防止内存泄露.
     */
    public abstract void onBefore(Disposable d);

    public abstract void onSuccess(T data);

    public void onError(String errorMsg) {
        ToastUtils.showShortToast(context, errorMsg);
    }

    public void onAfter() {}


    protected Disposable getDisposable() {
        return disposable;
    }

    @Override
    final public void onSubscribe(Disposable d) {
        this.disposable = d;
        this.onBefore(d);
    }

    @Override
    final public void onComplete() {
        this.onAfter();
    }

    @Override
    final public void onError(Throwable e) {
        e.printStackTrace();
        this.onError(NetUtils.analyzeNetworkError(context, e));
        this.onAfter();
    }

    @Override
    final public void onNext(T data) {
        this.onSuccess(data);
    }

}
