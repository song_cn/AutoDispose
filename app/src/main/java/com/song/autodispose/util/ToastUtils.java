package com.song.autodispose.util;

import android.content.Context;
import android.widget.Toast;


/**
 * 静态Toast
 */
public class ToastUtils {

	private static Toast toast = null;

	/**
	 * 显示短时间Toast
	 * @param txt 显示内容
     */
	public static void showShortToast(Context ctx, String txt) {
		if (toast == null) {
			toast = Toast.makeText(ctx, txt, Toast.LENGTH_SHORT);
		}
		toast.setText(txt);
		toast.show();
	}

	/**
	 * 显示长时间Toast
	 * @param txt 显示内容
	 */
	public static void showLongToast(Context ctx, String txt) {
		if (toast == null) {
			toast = Toast.makeText(ctx, txt, Toast.LENGTH_LONG);
		}
		toast.setText(txt);
		toast.show();
	}

	/** 取消显示 */
	public static void hideToast() {
		if (null != toast) {
			toast.cancel();
		}
	}

}