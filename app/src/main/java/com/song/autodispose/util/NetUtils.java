package com.song.autodispose.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.song.autodispose.R;
import com.song.autodispose.exception.ApiException;

import retrofit2.HttpException;


public class NetUtils {

    /**
     * 检查当前网络是否可用
     *
     * @return 是否连接到网络
     */
    public static boolean isNetworkAvailable(Context ctx) {
        ConnectivityManager connectivityManager = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager != null) {
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            if (info != null && info.isConnected()) {
                if (info.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String analyzeNetworkError(Context ctx, Throwable e) {
        //  String errMsg = UiUtils.getString(R.string.retry_after);
        String errMsg = ctx.getString(R.string.load_error);
        if (!NetUtils.isNetworkAvailable(ctx)) {
            errMsg = ctx.getResources().getString(R.string.retry_after);
        }
        if (e instanceof HttpException) {
            int state = ((HttpException) e).code();
            if (state == 403) {
                errMsg = ctx.getResources().getString(R.string.load_error);
            }
        }
        if (e instanceof ApiException) {
            errMsg = e.getMessage();
        }
        return errMsg;
    }

}
